from __future__ import unicode_literals

from django.db import models

    
class BioInfo(models.Model):
    client_id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=100)
    middlename = models.CharField(max_length=50, null=True)
    emailaddress = models.CharField(max_length=50, null=True)
    class Meta:
    	db_table = 'BioInfo'
