from django.shortcuts import render
from registration.forms import *
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext

# Create your views here.
@csrf_protect
def add(request):
    if request.method == 'POST':
        form = RecordForm(request.POST)
        if form.is_valid():
            BioInfo= form.save()
            return HttpResponseRedirect('/list/')
    else:
        form = RecordForm()
    return render(request, 'addRecord.html', {'form': form})

    
 
@csrf_protect
def list(request):
      clientinfo = BioInfo.objects.all()
      return render_to_response('viewRecord.html',{'clientinfo': clientinfo})

def home(request):
    return render_to_response('home.html')