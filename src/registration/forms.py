from django.utils.translation import ugettext_lazy

from django import forms

from .models import BioInfo

class RecordForm(forms.ModelForm):
    
    firstname = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only '}))
    middlename = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[A-Za-z ]+', 'title':'Enter Characters Only '}))
    emailaddress = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=("Email address")) 
    
    class Meta:
        model = BioInfo
        fields = '__all__'